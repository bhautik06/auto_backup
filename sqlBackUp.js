const cron = require('node-cron')
const moment = require('moment')
const exec  = require('child_process').exec

// cron.schedule('59 23 * * *', () => {
    const dbOptions = {
        host: 'localhost',
        user: 'root',
        password: '123',
        database: 'taxi_db'
    };
    
    let dateTime = moment().format('YYYY-MM-DD-HH-mm-DD')
    let filename = `database-backup/${dbOptions.database}_${dateTime}.sql`
    let cmd = `mysqldump -u ${dbOptions.user} -p${dbOptions.password} ${dbOptions.database} > ${filename}`

    console.log(`${dbOptions.database} - Backup Running... - `);
    exec(cmd, (err, stdout, stderr) => {
        if (err) {
            console.error(`exec error: ${err}`);
            return;
        }
        console.log('sucess!!');
        
    })
// });